package com.prodigious.grus.rest.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.jcr.Node;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;

import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

import com.google.gson.Gson;
import com.prodigious.grus.rest.model.request.Hello;

//import com.day.cq.wcm.api.Page;

//import cr.prodigious.aem.HelloService;

@SlingServlet(metatype = false, generateComponent = true, paths = "/services/grus/hello")
public class HelloServlet extends SlingAllMethodsServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8776569386442433683L;

	// @Reference
	// private HelloService helloService;
	
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		PrintWriter pw = response.getWriter();
		JSONObject result = new JSONObject();
		try {
			result.put("name", "Hello World Get!");
			result.put("servletName", getServletName());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String pathParam = request.getParameter("path");

		// ResourceResolver resolver = request.getResourceResolver();
		// Resource pageResource = resolver.getResource(pathParam);

		response.setContentType("application/json");
		pw.print(result);

		pw.flush();
		pw.close();
	}

	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		String pathParam = request.getParameter("path");
		String valueParam = request.getParameter("value");
		
		BufferedReader reader = request.getReader();
		Gson gson = new Gson();
		Hello hello = gson.fromJson(reader, Hello.class);

		PrintWriter pw = response.getWriter();
		JSONObject result = new JSONObject();
		
		try {
			result.put("name", "Hello World Post!");
			result.put("servletName", getServletName());
			result.put("request", hello);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// ResourceResolver resolver = request.getResourceResolver();
		// Resource pageResource = resolver.getResource(pathParam);

		response.setContentType("application/json");
		pw.print(result);

		pw.flush();
		pw.close();
	}
}
