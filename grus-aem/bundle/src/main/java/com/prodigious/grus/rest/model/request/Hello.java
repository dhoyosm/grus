package com.prodigious.grus.rest.model.request;

import java.io.Serializable;

public class Hello implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -518444949348321690L;
	
	private String t;

	public String getT() {
		return t;
	}

	public void setT(String t) {
		this.t = t;
	}

}
